// utilities contains a range of utility funtions to support the main
// eventmanager code

package eventmanager

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/go-redis/redis"
)

// GetLocation queries Redis to determine the location
//  Returns:
//    Latitude (float64)
//    Longitude (float64)
func GetLocation() (float64, float64) {
	lat, err := GetRedisVar("LATITUDE")
	if err != nil {
		Log.Warning(err)
		Log.Warning("Failed to retrieve Latitude")
		lat = "0"
	}

	lon, err := GetRedisVar("LONGITUDE")
	if err != nil {
		Log.Warning(err)
		Log.Warning("Failed to retrieve Longitude")
		lon = "0"
	}

	fLat, _ := strconv.ParseFloat(lat, 64)
	fLong, _ := strconv.ParseFloat(lon, 64)

	return fLat, fLong
}

// GetRedisVar queries Redis for a value associated with a key
//  Returns:
//  String representation of value and error
func GetRedisVar(s string) (r string, err error) {

	// Open a Redis client
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       1,
	})

	//  Get result from Redis
	r, err = client.Get(s).Result()
	client.Close()

	return
}

// SetRedisVar sets a key/value pair in Redis
//   k: key (string)
//   v: value (string)
//  Returns: error
func SetRedisVar(k string, v string) (r error) {

	var x string

	// Open a Redis client
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       1,
	})

	if k[0] == '%' {
		x = strings.TrimPrefix(strings.TrimSuffix(k, "%"), "%")
	} else {
		x = k
	}

	r = client.Set(x, v, 0).Err()
	client.Close()

	return
}

// SetRedisAction posts a new action to the action queue in Redis
//   r: rule (action) to post
//  Returns: error
func SetRedisAction(t, n, c, v string) (err error) {
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       2,
	})

	action := fmt.Sprintf("{\"type\":\"%s\",\"node\":\"%s\",\"class\":\"%s\",\"value\":\"%s\"}", t, n, c, v)
	err = client.RPush("actions", action).Err()

	return
}

// EvalVariables compares left and right variables for a Condition
//   n: Left variable name (stored in Redis)
//   v: Right variable name (literal or Redis variable).  Redis variables denoted by % prefix/suffix
//   c: Camparison type (=, !=, <, >)
//  Returns: bool
func EvalVariables(n, v, c string) (o bool) {

	var l, r, x, y string
	var err error

	// Assume the outcome is false
	o = false

	// Determine left operand
	if n[0] == '%' {
		x = strings.TrimPrefix(strings.TrimSuffix(n, "%"), "%")
		l, err = GetRedisVar(x)
		if err != nil {
			Log.Warningf("Left variable in comp not found in Redis: %s", x)
			return
		}
	} else {
		l = n
	}

	// Determine right operand
	if v[0] == '%' {
		y = strings.TrimPrefix(strings.TrimSuffix(v, "%"), "%")
		r, err = GetRedisVar(y)
		if err != nil {
			Log.Warningf("Right variable in comp not found in Redis: %s", y)
			return
		}
	} else {
		r = v
	}

	switch c {
	case "=", "EQUAL", "equal":
		{
			// Test to see if left and right are the same
			if l == r {
				o = true
			}
		}

	case "!=", "NOTEQUAL", "notequal":
		{
			// Test to see if left and right are not the same
			if l != r {
				o = true
			}
		}

	case ">", "GREATERTHAN", "greaterthan":
		{
			// Test to see if left is > right
			if l > r {
				o = true
			}
		}

	case "<", "LESSTHAN", "lessthan":
		{
			// Test to see if left < right
			if l < r {
				o = true
			}
		}

	default:
		Log.Errorf("Invalid variable evaluation: %s", c)

	}
	Log.Debugf("%s (%s) %s %s(%s) is %t", n, l, c, y, r, o)

	return
}

// GetZNodeName takes an integer node ID and returns the nice name
func GetZNodeName(z int) string {
	for _, v := range IONodes.zwave {
		if v.address == z {
			return v.name
		}
	}

	//  Didn't find zwave node
	Log.Warningf("Received Zwave event/notification from unknow node: %i", z)
	return "xxxxx"
}
