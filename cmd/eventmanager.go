package main

import (
	"os"

	eventmanager "bitbucket.org/broo_myster/go-eventmanager"
	"github.com/galdor/go-cmdline"
	"github.com/go-redis/redis"
	logging "github.com/op/go-logging"
)

var logLevel = logging.INFO
var configFile = "./config/config.json"

func main() {
	// Parse command line arguments
	// config file path
	// log level
	cmdLine := cmdline.New()
	cmdLine.AddOption("c", "config", "file", "Full path and name of config file (default /etc/eventmanager/config.json)")
	cmdLine.AddFlag("v", "verbose", "Verbose/Debug output")
	cmdLine.Parse(os.Args)

	if cmdLine.IsOptionSet("v") {
		logLevel = logging.DEBUG
	}

	if cmdLine.IsOptionSet("c") {
		configFile = cmdLine.OptionValue("c")
	}

	// Set up levelled logging
	eventmanager.LogLevel = logLevel
	eventmanager.Log = eventmanager.SetupLogger()

	// Check that Redis is alive
	client := redis.NewClient(&redis.Options{
		Addr:     "redis:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	_, err := client.Ping().Result()
	if err != nil {
		eventmanager.Log.Critical("Can't connect to Redis:", err)
		os.Exit(1)
	}

	// Load and validate config (nodes, scenarios)
	eventmanager.ConfigFile = configFile
	err = eventmanager.LoadConfig(configFile)
	if err != nil {
		eventmanager.Log.Critical(err)
		os.Exit(1)
	}

	// Set up timer map
	TMap := eventmanager.NewTimerMap()
	TMap.Clear()

	//  Start action listener loop
	go eventmanager.ActionListener()

	// Start main rule evaluation loop and event listener
	eventmanager.RuleLoop()
}
