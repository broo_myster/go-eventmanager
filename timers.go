// timers implements a map of periodic (e.g. 60 seconds), relative (e.g. 30mins before sunrise)
// or absolute (e.g. 22:30) timer functions.  Timers are created and checked every second.
// Expired timers are deleted (one-shot) or reinstated (recurring) and an expiry event is
// posted to a Redis "events" queue.

// TODO: add test functions

package eventmanager

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/cpucycle/astrotime"
	"github.com/go-redis/redis"
)

const (
	cONCE      = -1
	cALLDAYS   = 0x00
	cMONTHDAY  = 0x1f // Placeholder 0x01 to 0x1f (31) = day of month
	cSUNDAY    = 0x20
	cMONDAY    = 0x21
	cTUESDAY   = 0x22
	cWEDNESDAY = 0x23
	cTHURSDAY  = 0x24
	cFRIDAY    = 0x25
	cSATURDAY  = 0x26
	cEVENDAYS  = 0x27
	cODDDAYS   = 0x28
	cWEEKDAYS  = 0x29
	cWEEKENDS  = 0x2a
)

// Timers holds the details for a single timer
//   expireTime: The Time construct for the timer expiry
//   recurrenceSpec: Specify recurrence attributes (e.g. daily, weekend, etc)
//   timerSetupString: Setup string for absolute timers (e.g. SS+0h15m, 22:30, etc)
//   timerType: "period" for timed event or "timer" for clock-based event
//   ruleIndex: On expiry this is the next rule to be executed in the scenario
//              associated with the timer
type Timers struct {
	expireTime       time.Time
	recurrenceSpec   string
	timerSetupString string
	timerType        string
	ruleIndex        int
}

// TimerMap is a map of Timer structs with the timer identifier as the index.
// This should be called as the constructor for a timer capability
type TimerMap map[int]Timers

// TMap is the global pointer for the timer map
var TMap TimerMap

// Latitude : Imported global variable for latitude of running instance
var Latitude float64

// Longitude : Imported global variable for latitude of running instance
var Longitude float64

// NewTimerMap builds a new timer map and returns the pointer
func NewTimerMap() TimerMap {
	TMap = make(map[int]Timers)

	// get lat long from Redis
	Latitude, Longitude = GetLocation()

	// Set up a go routine to check for expired timers
	go checkTimers(TMap)

	// And return the reference to the timer map
	return TMap
}

// CreateTimer starts a new one-shot or recurring timer
//  timerID:  identifier for the new timer (usually the scenario ID it's associated with)
//  recurring:     recurrence spec - e.g. daily, weekdays, DoM, etc.
//  setup:    the setup string used to reactivate recurring timers
//  ruleIndex: next rule to be executed in scenario defined by timerID
func (t TimerMap) CreateTimer(timerID int, rec string, ruleIndex int, setup string) error {

	var expiry time.Time

	recurring := recDecode(rec)

	if recurring < cONCE || recurring > cWEEKENDS {
		// Invalid recurrence spec
		Log.Errorf("Invalid timer recurrence spec: %d", recurring)
		return errors.New("Invalid timer spec.  Check log")
	}

	if strings.Contains(setup, "SR") {
		// Is this timer relative to sunrise?
		offset := setup[2:]
		expiry = solarTrans(true, offset)

	} else if strings.Contains(setup, "SS") {
		// ...or sunset?
		offset := setup[2:]
		expiry = solarTrans(false, offset)

	} else {
		// ...or an absolute time?
		expiry = time.Now()
		minutes, _ := strconv.Atoi(setup[3:])
		hours, _ := strconv.Atoi(setup[:2])

		//  TODO: There must be a more elegant way of setting a time structure.
		//        Can't see how without building a fully formatted time/date string.
		//        This section of code takes the current time, retains the date, and
		//        increments minutes and hours until they match the timer spec.

		// Set seconds to zero
		for expiry.Second() != 0 {
			expiry = expiry.Add(time.Second)
		}

		// Match minutes
		for minutes != expiry.Minute() {
			expiry = expiry.Add(time.Minute)
		}

		// Match hours
		for hours != expiry.Hour() {
			expiry = expiry.Add(time.Hour)
		}
	}

	// If the timer is due to expire in less than 2 minutes, set for tomorrow
	if expiry.Sub(time.Now()).Seconds() <= 60 {
		expiry = expiry.Add(time.Hour * 24)
	}

	if recurring > cONCE {
		// Find the right day for the timer
		for day := 1; day <= 62; day++ {
			if recurring == cALLDAYS {
				break
			}

			// daySpec is a date, so keep cycling until we match day of month
			if recurring <= cMONTHDAY && recurring == expiry.Day() {
				break
			}

			// daySpec is a day of the week, so keep cycling until we match the day of week
			if recurring <= cSATURDAY && recurring == (int(expiry.Weekday())+32) {
				break
			}

			// daySpec is an even date
			if recurring == cEVENDAYS && (expiry.Day()%2 == 0) {
				break
			}

			// daySpec is an odd date
			if recurring == cODDDAYS && (expiry.Day()%2 != 0) {
				break
			}

			// daySpec is a weekday
			if recurring == cWEEKDAYS && (expiry.Weekday() >= 1) && (expiry.Weekday() <= 5) {
				break
			}

			// dayspec is a weekend
			if recurring == cWEEKENDS {
				if expiry.Weekday() == 0 || expiry.Weekday() == 6 {
					break
				}

				// Are we out of options?  Need to account for MONTHDAY = 31 but current month has only 30 days (so need to
				// cycle through next month).
				if day == 62 {
					Log.Error("Reached end of recurrence window without resolution")
					return errors.New("Reached end of recurrence window without resolution")
				}
			}

			expiry = expiry.Add(time.Hour * 24)
		}
	}

	t[timerID] = Timers{
		expiry,
		rec,
		setup,
		"timer",
		ruleIndex}

	Log.Infof("Setting up timer 0x%X: %s %s, expiring at %s", timerID, setup, rec, expiry)
	return nil
}

// CreatePeriodTimer starts a new one-shot or recurring timer
//  timerID:  identifier for the new timer (usually the rule ID it's associated with)
//  period:   time in seconds until expiry
//  recurring:  -1 (once) or the period used to reactivate recurring timers
func (t TimerMap) CreatePeriodTimer(timerID int, recurring string, ruleIndex int, period string) error {

	p, err := strconv.Atoi(period)
	if err != nil {
		p = 0
	}

	if p > 5 {
		t[timerID] = Timers{
			time.Now().Add(time.Second * time.Duration(p)),
			recurring,
			period,
			"period",
			ruleIndex}

		Log.Infof("Setting up period timer 0x%X: %s secs, next index: %d", timerID, period, ruleIndex)
		return nil
	}

	Log.Warningf("Period timer < 5 seconds.  Skipping")
	return errors.New("Period timer < 5 seconds.  Skipping")

}

// Clear clears all entries in the active timer list.
func (t TimerMap) Clear() {
	for timerID := range t {
		delete(t, timerID)
	}
}

// TimerExists returns TRUE/FALSE based on whether the timer id exists
//  timerID: identifier for the timer to be checked
func (t TimerMap) TimerExists(timerID int) bool {
	_, ok := t[timerID]
	return ok
}

// TimerLife returns the number of seconds until timer expires
//  timerID:  identifier for the new timer (usually the scenario ID it's associated with)
func (t TimerMap) TimerLife(timerID int) time.Duration {
	expiry := t[timerID].expireTime
	return expiry.Sub(time.Now())
}

// CheckTimers loops through all active timers once a second and runs a garbage
//  collect to delete timers or restart recurring timers
func checkTimers(t TimerMap) {

	// Loop forever
	for {
		//  Cycle through all active timers
		for timerID := range t {
			expiry := t[timerID].expireTime

			//  Check to see if the timer has expired
			if expiry.Sub(time.Now()).Seconds() <= 0 {

				Log.Infof("Timer 0x%X expired", timerID)

				// Copy the timer record that has expired
				recurrence := t[timerID].recurrenceSpec
				setup := t[timerID].timerSetupString
				ruleIndex := t[timerID].ruleIndex
				timerType := t[timerID].timerType

				// Fire an expiry event to Redis
				err := fireTimerEvent(timerID, ruleIndex)
				if err != nil {
					Log.Error(err)
				}

				// Delete the expired timer
				t.deleteTimer(timerID)

				// If it's a recurring timer, set it up again
				if recDecode(recurrence) > cONCE {
					if timerType == "set-period" {
						t.CreatePeriodTimer(timerID, recurrence, ruleIndex, setup)
					} else {
						t.CreateTimer(timerID, recurrence, ruleIndex, setup)
					}
				}
			}
		}

		//	Log.Debug("Timer Check")
		time.Sleep(1000 * time.Millisecond)
	}
}

// DeleteTimer deletes an active timerID
//  timerID:  identifier for the timer to delete
func (t TimerMap) deleteTimer(timerID int) {
	delete(t, timerID)
}

// solarTrans returns the next sunrise or sunset time based on location
//   r:  boolean where sunrise = TRUE and sunset = false
//   shift:  offset to add/substrct from transition time
//           - must be of the form "0h15m" or ""-1h23m"
//   lat/long:  lattitude/longitude to use for solar transition time
func solarTrans(r bool, shift string) (t time.Time) {

	// Get base sunrise/sunset time
	if r == true {
		t = astrotime.NextSunrise(time.Now(), Latitude, Longitude)
	} else {
		t = astrotime.NextSunset(time.Now(), Latitude, Longitude)
	}

	// Add/subtract time shift
	d, err := time.ParseDuration(shift)
	if err != nil {
		d = 0
	}

	t = t.Add(d)

	return
}

// fireTimerEvent places a new timer expiry event on the Redis queue
func fireTimerEvent(id, index int) (err error) {
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})

	event := fmt.Sprintf("{\"type\": \"timer\",\"data\":{\"rule\":\"%d\",\"index\":\"%d\"}}", id, index)
	err = client.RPush("events", event).Err()

	client.Close()
	return
}

// decode recurrent string into an integer
func recDecode(r string) (d int) {
	r = strings.ToUpper(r)
	switch r {
	case "ONCE":
		d = cONCE

	case "ALL":
		d = cALLDAYS

	case "SUNDAY", "SUN":
		d = cSUNDAY

	case "MONDAY", "MON":
		d = cMONDAY

	case "TUESDAY", "TUE":
		d = cTUESDAY

	case "WEDNESDAY", "WED":
		d = cWEDNESDAY

	case "THURSDAY", "THU":
		d = cTHURSDAY

	case "FRIDAY", "FRI":
		d = cFRIDAY

	case "SATURDAY", "SAT":
		d = cSATURDAY

	case "EVEN":
		d = cEVENDAYS

	case "ODD":
		d = cODDDAYS

	case "WEEKDAY":
		d = cWEEKDAYS

	case "WEEKEND":
		d = cWEEKENDS

	default:
		// Day of the month
		{
			s, err := strconv.Atoi(r)
			if err != nil {
				d = cONCE
			} else {
				d = s
			}
		}
	}

	return
}
