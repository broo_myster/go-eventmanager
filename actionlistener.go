// actionlistener watches the Redis action queue for actionable events.  When
// an action appears on the queue, the actionlistener go routine determines what
// type of action it is and executes as appropriate.

package eventmanager

import (
	"encoding/json"
	"fmt"
	"net"
	"strings"
	"time"

	slack "github.com/ashwanthkumar/slack-go-webhook"
	"github.com/go-redis/redis"
)

// ActionListener checks the Redis queue for actions and executes as appropriate
func ActionListener() {
	for {

		client := redis.NewClient(&redis.Options{
			Addr:     "localhost:6379",
			Password: "",
			DB:       2,
		})

		// Check that there are events to process
		l, err := client.LLen("actions").Result()

		if err != nil {
			Log.Errorf("Unable to check action queue: %s", err)
		} else {
			if l > 0 {
				// Process actions
				for i := 0; i < int(l); i++ {

					e, _ := client.LPop("actions").Result()
					Log.Infof("Popped action: %s", e)

					manageActions(e)

				}
			}
		}

		client.Close()
		time.Sleep(80 * time.Millisecond)
	}
}

func manageActions(e string) {
	// Check and parse the action JSON
	var m map[string]interface{}

	err := json.Unmarshal([]byte(e), &m)
	if err != nil {
		// Can't parse the JSON
		Log.Errorf("Malformed action. Discarding: %s", e)
		return
	}

	switch m["type"] {
	case "act-slack":
		sendSlack(m)

	case "act-zwave":
		sendZwave(m)

	default:
		{
			Log.Errorf("Unmanaged action type: %s", m["type"])
		}
	}

}

// Send ZWave command
//  m: map of action strings
func sendZwave(m map[string]interface{}) {
	// Get ZWave socket details
	for _, s := range IONodes.web {
		if s.name == "zwave server" {
			w := s.address

			Log.Debug("Opening a socket connection to", w)
			conn, err := net.DialTimeout("tcp", w, 5*time.Second)
			if err != nil {
				Log.Debugf("Error opening socket to ZWave server at %s: %s", w, err)
				return
			}

			// Get a zwave compatible value for the action
			var v string
			if m["value"] == "ON" {
				v = "99"
			} else if m["value"] == "OFF" {
				v = "0"
			} else {
				v = derefText(m["value"].(string))
			}

			// Collect node details
			for _, s := range IONodes.zwave {
				if s.name == m["node"].(string) {
					n := s.address
					i := s.instance
					t := s.ztype
					Log.Debugf("Sending ZWave command: DEVICE~%d~%d~%s~%s", n, i, v, t)
					fmt.Fprintf(conn, "DEVICE~%d~%d~%s~%s", n, i, v, t)
				}
			}
			//conn.Close()
		}
	}
}

// Send Slack notification
//  m: map of action strings
func sendSlack(m map[string]interface{}) {

	// Get Slack username
	u, err := GetRedisVar("SLACK_USER")
	if err != nil {
		u = "SHARC"
	}

	// Set up playload
	payload := slack.Payload{
		Text:     derefText(m["value"].(string)),
		Username: u,
		Channel:  m["node"].(string),
	}

	// Get Slack webhook details and credentials
	for _, s := range IONodes.web {
		if s.name == "slack" {
			w := s.address

			err := slack.Send(w, "", payload)
			if err != nil {
				Log.Errorf("Error posting Slack notification: %s", err)
			}
		}
	}
}

// Find any variables in the text string and replace them with values from Redis
// or special characters
func derefText(t string) string {
	var s string

	// If there are no variables, just return the string
	i := strings.Index(t, "%")
	if i < 0 {
		return t
	}

	// Tokenise the string, breaking at %, and put the results in a string array
	x := strings.Split(t, "%")

	for i := 0; i < len(x); i++ {
		// Even indexes will now contain text and odd indexes will be variables
		if i%2 != 0 {
			s += rebuildVariable(x[i])
		} else {
			s += x[i]
		}
	}

	return s
}

// Look for special case variables or collect a variable from Redis
func rebuildVariable(t string) string {
	switch t {
	case "PERC":
		return "%"

	case "PIPE":
		return "|"

	default:
		{
			x, _ := GetRedisVar(t)
			return x
		}
	}
}
