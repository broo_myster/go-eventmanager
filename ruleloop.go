// ruleloop is the primary loop for event manager.  Rule loop looks for events
// in the Redis event queue and checks for scenarios expecting those events.  Rules
// within the scenarios are executed where appropriate, including the instantiation
// of timers, setting/checking variables, and firing actions to the Redis action
// queue.

package eventmanager

import (
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/go-redis/redis"
)

const (
	cIfEvent     = "if-event"
	cIfTimer     = "if-timer"
	cIfPeriod    = "if-period"
	cIfVariable  = "if-variable"
	cSetVariable = "set-variable"
	cDummy       = "dummy"
)

// RuleLoop is the primary evaluation loop.  It looks for event triggers, assesses
// scenarios, and executes rules within scenarios
func RuleLoop() {
	for {
		// Check event queue and process
		processEvents()

		// Loop through scenarios and process
		processScenarios()

		//Log.Debug("Rule Loop")
		time.Sleep(100 * time.Millisecond)
	}
}

// processEvents looks for events stored in teh Redis queue and processes
func processEvents() {

	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})

	// Check that there are events to process
	l, err := client.LLen("events").Result()

	if err != nil {
		Log.Errorf("Unable to check event queue: %s", err)
	} else {
		if l > 0 {
			// Process events
			for i := 0; i < int(l); i++ {

				e, _ := client.LPop("events").Result()
				Log.Debugf("Popped event: %s", e)
				// Dedupe
				client.LRem("events", 0, e)

				// Check for commands
				// Check and set variables
				// Check for trigger events
				manageEvents(e)

			}
		}
	}

	client.Close()
}

// parse and deal with events
func manageEvents(e string) {

	var m map[string]interface{}
	err := json.Unmarshal([]byte(e), &m)
	if err != nil {
		// Can't parse the JSON
		Log.Errorf("Malformed event. Discarding: %s", e)
		return
	}

	// We have a valid JSON event.  Process based on type
	var d = m["data"].(map[string]interface{})

	switch m["type"] {
	case "command":
		processEvCommand(d)

	case "timer":
		processEvTimer(d)

	case "zwave":
		processEvZwave(d)

	default:
		Log.Warningf("Unknown event.  Discarding: %s", e)
	}

	return

}

// Deal with timer expiry events
func processEvTimer(d map[string]interface{}) {
	// Check for valid parameters
	if _, ok := d["rule"]; ok {
		if _, ok := d["index"]; ok {
			r, _ := strconv.Atoi(d["rule"].(string))
			j, _ := strconv.Atoi(d["index"].(string))

			for i, sc := range Scenarios {
				if r == sc.id {
					// Update rule associated with expired timer to point to the next rule
					Scenarios[i].currentIndex = j
					Log.Debug("Rule ID", sc.id, "index set to", sc.currentIndex, "of", len(sc.rules), ":", sc.rules[sc.currentIndex])

					break
				}
			}
		}
	}
}

// Deal with command events
func processEvCommand(d map[string]interface{}) {
	// Check for valid parameters
	if _, ok := d["command"]; ok {
		switch d["command"] {
		case "RELOAD", "reload":
			{
				// Load and validate config (nodes, scenarios)
				Log.Warningf("Reloading Config: %s", ConfigFile)
				err := LoadConfig(ConfigFile)
				if err != nil {
					Log.Critical(err)
					os.Exit(1)
				}

				// Set up timer map
				TMap := NewTimerMap()
				TMap.Clear()
			}

		case "EXIT", "exit", "quit", "QUIT":
			{
				Log.Info("Exit command received. Terminating.")
				os.Exit(0)
			}
		}
	}
}

// process incoming ZWave events
func processEvZwave(d map[string]interface{}) {
	// Parse incoming Zwave events and notifications
	if _, ok := d["node"]; ok {
		if _, ok := d["class"]; ok {
			if _, ok := d["value"]; ok {
				// Valid JSON.  process
				n, _ := strconv.Atoi(d["node"].(string))
				v := GetZNodeName(n)
				c := d["class"].(string)

				if _, ok := d["label"]; ok {
					c = fmt.Sprintf("%s:%s", c, d["label"])
				}

				// k is the Redis key (variable) name (excludes "COMMAND_CLASS_")
				k := fmt.Sprintf("%s:%s", v, c[14:])
				x := d["value"].(string)

				Log.Debug(n, v, c)

				// Store Zwave event in Redis
				Log.Infof("Setting Redis variable %s to %s", k, x)
				SetRedisVar(k, x)

				// Cycle through scenarios to see if any rules are triggered
				for i, sc := range Scenarios {
					if sc.rules[sc.currentIndex].rType == cIfEvent &&
						sc.rules[sc.currentIndex].node == v &&
						sc.rules[sc.currentIndex].class == c &&
						sc.rules[sc.currentIndex].value == x {
						if sc.rules[sc.currentIndex].pOr == true {
							Scenarios[i].currentIndex += 2
						} else {
							Scenarios[i].currentIndex++
						}

					}

					// Check for matching battery report
					if sc.rules[sc.currentIndex].rType == cIfEvent &&
						sc.rules[sc.currentIndex].node == v &&
						sc.rules[sc.currentIndex].class == c &&
						c == "COMMAND_CLASS_BATTERY" {
						Scenarios[i].currentIndex++
					}

					// Check to see if the event retriggers an active scenario
					if sc.rules[0].rType == cIfEvent &&
						sc.rules[0].node == v &&
						sc.rules[0].class == c &&
						sc.rules[0].value == x {
						Scenarios[i].currentIndex = 1
						Log.Debugf("Retrigger event detected for rule 0x%X", sc.id)
						TMap.deleteTimer(sc.id)
					}
				}
			}
		}
	}
}

// processScenarios loops through all scenarios and executes actions and tests
func processScenarios() {
	// Loop to check all scenarios
	for _, sc := range Scenarios {
		Log.Debug(sc.id, ":", sc.currentIndex, "=>", sc.rules[sc.currentIndex])
	}

	for i, sc := range Scenarios {

	L:
		for {

			// Keep iterating over rules in a scenario until:
			//  1) the end of the ruleset is reached
			//  2) a test returns false
			//  3) we're waiting on a timer
			//  4) an error occurs

			t := Scenarios[i].rules[Scenarios[i].currentIndex].rType
			c := Scenarios[i].rules[Scenarios[i].currentIndex].class
			v := Scenarios[i].rules[Scenarios[i].currentIndex].value
			n := Scenarios[i].rules[Scenarios[i].currentIndex].node

			// Evaluate/Execute rule based on type
			switch t {
			case cIfTimer:
				// Check if timer is running and configure if not
				{
					if !TMap.TimerExists(sc.id) {
						TMap.CreateTimer(sc.id, c, Scenarios[i].currentIndex+1, v)
					}
					break L
				}

			case cIfPeriod:
				// Set new period timer and overwrite existing timer if it exists
				{
					if !TMap.TimerExists(sc.id) {
						TMap.CreatePeriodTimer(sc.id, c, Scenarios[i].currentIndex+1, v)
					}
					break L
				}

			case cIfVariable:
				// Check variable and move to next rule if TRUE
				{
					if EvalVariables(n, v, c) {
						// Condition is true.  Move to next rule in scenario
						Scenarios[i].currentIndex++
					} else {
						// Condition is false, so reset the rule
						Scenarios[i].currentIndex = 0
						break L
					}
				}

			case cSetVariable:
				// Set a variable
				{
					err := SetRedisVar(n, v)
					if err != nil {
						Log.Error("Unable to set Redis variable: ", n, v)
						Scenarios[i].currentIndex = 0
						break L
					}
					Scenarios[i].currentIndex++
				}

			case cIfEvent:
				// We can skip this because events are processed when they arrive
				{
					break L
				}

			case "act-zwave", "act-slack", "act-web", "act-relay", "act-serial":
				//send action to action queue
				{
					SetRedisAction(t, n, c, v)
					Log.Info("Action:", t, n, c, v)
					Scenarios[i].currentIndex++
				}

			case cDummy:
				// Do nothing
				break L

			default:
				{
					Log.Errorf("Unknown rule type: %s", t)
					break L
				}
			}

			// Check to see if we've reached the end of the rule

			Log.Debug("Rule:", i, "Current:", Scenarios[i].currentIndex, ", Length:", len(Scenarios[i].rules))
			if Scenarios[i].currentIndex >= len(Scenarios[i].rules) {
				// We've completed the full rule set.  Reset the scenario
				Scenarios[i].currentIndex = 0
				break L
			}
		}
	}
}
