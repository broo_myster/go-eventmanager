// processconfig parses the config file at startup.  Variables are loaded into
// Redis. Nodes and types are loaded into memory.  Rules are validated and
// loaded into memory.

package eventmanager

import (
	"encoding/json"
	"io/ioutil"
	"strconv"
)

// Nodes holds the low level detail of an individual IO node
type Nodes struct {
	zwave  []znode
	relay  []rnode
	web    []wnode
	serial []snode
	input  []inode
}

// Zwave nodes
type znode struct {
	name     string
	address  int
	instance int
	ztype    string
}

// Relay nodes
type rnode struct {
	name    string
	address int
	active  int
}

// Serial nodes
type snode struct {
	name    string
	address string
}

// Web nodes
type wnode struct {
	name    string
	address string
}

// Input nodes
type inode struct {
	name    string
	address string
}

// Rule holds the details for an individual rule in a given scenario
type Rule struct {
	index int
	rType string
	node  string
	class string
	value string
	pOr   bool
}

// Scenario contains a list of rules for that scenario
type Scenario struct {
	id           int
	name         string
	currentIndex int
	rules        []Rule
}

// Scenarios holds a full list of scenarios and associated rules
var Scenarios []Scenario

// IONodes is a struct with all of the addressable nodes
var IONodes Nodes

// ConfigFile is the global file/path for application config
var ConfigFile = "./config/config.json"

// NewScenarioMap builds a new map of automation scenarios and returns the pointer
//func NewScenarioMap() ScenarioMap {
//	s := make(map[int]Scenarios)

//	return s
//}

// LoadConfig reads the JSON config file into memory maps and Redis (for variables)
//   nodeMap: a pointer to a map of IO nodes that eventmanager can interact with
//   scenarioMap: a pointer to a map of automation scenarios and associated rules
//   configFile: filename containing the JSON formatted nodes and scenarios
//  Returns: nil or an error indicating that there was an issue loading and processing
//   the configuration file.
func LoadConfig(configFile string) (err error) {

	var c interface{} //The interface to be used for the config

	// Open & read config file
	raw, err := ioutil.ReadFile(configFile)
	if err != nil {
		return
	}

	// Extract JSON from byte stream
	err = json.Unmarshal(raw, &c)
	if err != nil {
		Log.Errorf("Failed to parse config JSON: %s", err)
		return
	}

	// Parse and process the JSON configuration
	m := c.(map[string]interface{})
	for k, v := range m {

		switch k {
		case "nodes":
			storeNodes(v)

		case "init":
			storeVars(v)

		case "triggers":
			storeScenarios(v)
		}
	}

	return
}

// storeVars loads initialisation variables into Redis
func storeVars(m interface{}) (err error) {

	// Prepare variables
	var valuestring string
	n := m.([]interface{})

	//  Extract variables from map`
	for _, v := range n {
		val, _ := v.(map[string]interface{})

		name := val["name"].(string)
		value := val["value"]

		// We can't be sure what type the variable is and Redis wants a string value
		switch value.(type) {
		case float64:
			valuestring = strconv.FormatFloat(value.(float64), 'f', -1, 64)

		case int:
			valuestring = strconv.Itoa(value.(int))

		default:
			valuestring = value.(string)
		}

		// Post to Redis
		err = SetRedisVar(name, valuestring)
		if err != nil {
			Log.Warning(err)
			Log.Warning("Failed to post variable %s to Redis", name)
			return
		}
	}

	return
}

// storeNodes parses node data and stores in memory
func storeNodes(m interface{}) (err error) {

	k := m.([]interface{})

	for _, v := range k {
		n, _ := v.(map[string]interface{})

		if _, ok := n["zwave"]; ok {
			parseZwave(n)
		}

		if _, ok := n["relay"]; ok {
			parseRelay(n)
		}

		if _, ok := n["web"]; ok {
			parseWeb(n)
		}

		if _, ok := n["serial"]; ok {
			parseSerial(n)
		}

		if _, ok := n["input"]; ok {
			parseInput(n)
		}
	}

	return
}

// Unpack and store ZWave node details
func parseZwave(n map[string]interface{}) {
	m := n["zwave"].([]interface{})

	for _, v := range m {
		x := v.(map[string]interface{})
		var z znode
		z.name = x["name"].(string)
		z.address = int(x["address"].(float64))
		z.instance = int(x["instance"].(float64))
		z.ztype = x["type"].(string)

		IONodes.zwave = append(IONodes.zwave, z)
	}
}

// Unpack and store relay node details
func parseRelay(n map[string]interface{}) {
	m := n["relay"].([]interface{})

	for _, v := range m {
		x := v.(map[string]interface{})
		var r rnode
		r.name = x["name"].(string)
		r.address = int(x["address"].(float64))
		r.active = int(x["active"].(float64))

		IONodes.relay = append(IONodes.relay, r)
	}
}

// Unpack and store serial/tty node details
func parseSerial(n map[string]interface{}) {
	m := n["serial"].([]interface{})

	for _, v := range m {
		x := v.(map[string]interface{})
		var s snode
		s.name = x["name"].(string)
		s.address = x["address"].(string)

		IONodes.serial = append(IONodes.serial, s)
	}
}

// Unpack and store web/socket service details
func parseWeb(n map[string]interface{}) {
	m := n["web"].([]interface{})

	for _, v := range m {
		x := v.(map[string]interface{})
		var w wnode
		w.name = x["name"].(string)
		w.address = x["address"].(string)

		IONodes.web = append(IONodes.web, w)
	}
}

// Unpack and store input node details
func parseInput(n map[string]interface{}) {
	m := n["input"].([]interface{})

	for _, v := range m {
		x := v.(map[string]interface{})
		var i inode
		i.name = x["name"].(string)
		i.address = x["address"].(string)

		IONodes.input = append(IONodes.input, i)
	}
}

// storeScenarios parses scenarios and rules and stores in memory
func storeScenarios(m interface{}) (err error) {
	k := m.([]interface{})

	// Loop through all scenarios
	for sc, v := range k {

		// Clear scenario struct for new scenario
		scenario := Scenario{}
		n := v.(map[string]interface{})
		r := n["rules"].([]interface{})

		scenario.id = sc + 0x1000
		scenario.name = n["name"].(string)
		scenario.currentIndex = 0

		// Loop through each rule of each scenario
		for index, x := range r {

			// Clear rule struct for new scenario
			rule := Rule{}

			y := x.(map[string]interface{})
			Log.Debug(scenario.id, scenario.name, index, y)

			rule.index = index
			rule.rType = y["type"].(string)

			if val, ok := y["node"]; ok {
				rule.node = val.(string)
			}

			if val, ok := y["class"]; ok {
				rule.class = val.(string)
			}

			if val, ok := y["value"]; ok {
				rule.value = val.(string)
			}

			if val, ok := y["pOr"]; ok {
				rule.pOr = val.(bool)
			}

			scenario.rules = append(scenario.rules, rule)
		}

		Scenarios = append(Scenarios, scenario)
	}

	return
}
