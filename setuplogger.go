// setuplogger implements a global logging function and sets up levels and format

package eventmanager

import (
	"os"

	logging "github.com/op/go-logging"
)

// LogLevel is the debug level imported from main
var LogLevel logging.Level

// Log is the global pointer to the logger
var Log *logging.Logger

// SetupLogger starts a logging backend and returns a pointer to the Logger
func SetupLogger() *logging.Logger {

	// Set up logging levels and format
	var log = logging.MustGetLogger("eventManager")

	var format = logging.MustStringFormatter(
		`%{color}%{time:15:04:05.000} %{shortfunc} ▶ %{level:.4s} %{id:03x}%{color:reset} %{message}`,
	)
	logBackend := logging.NewLogBackend(os.Stderr, "", 0)
	backendFormatter := logging.NewBackendFormatter(logBackend, format)
	backendLeveled := logging.AddModuleLevel(backendFormatter)
	backendLeveled.SetLevel(LogLevel, "")
	logging.SetBackend(backendLeveled)

	return log
}
